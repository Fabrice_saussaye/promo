import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, FormArray, ValidatorFn, ValidationErrors, AbstractControl } from '@angular/forms';
import { PromotionModel } from '../models/promotion.model';
import { PromotionService } from '../services/promotion.service';
import { Router } from '@angular/router';
import { ThresholdModel } from '../models/threshold.model';
import { DiscountItemModel } from '../models/discount-item.model';
import { DiscountType } from '../constants/discountType.enum';

@Component({
    selector: 'app-promotion-add',
    templateUrl: './promotion-add.component.html',
    styleUrls: ['./promotion-add.component.css']
})
export class PromotionAddComponent implements OnInit {

    static formNames = {
        thresholds: 'thresholds',
        minAmount: 'minAmount',
        maxAmount: 'maxAmount',
        shippingDiscountRate: 'shippingDiscountRate',
        serviceDiscountRate: 'serviceDiscountRate'
    };

    promotionForm: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private promotionService: PromotionService,
        private router: Router) { }

    ngOnInit() {

        this.initForm();
    }

    initForm() {

        this.promotionForm = this.formBuilder.group(
        {
            promoCodeId: ['', [Validators.required, Validators.pattern('[0-9]*')]],
            promotionCode: ['', [Validators.required, Validators.pattern('[0-9A-Z]*')]],
            name: ['', Validators.required],
            enable: [false],
            startValidityDate: ['', [Validators.required]],
            endValidityDate: ['', [Validators.required]],
            thresholds: this.formBuilder.array([])
        },{
            validators: [this.validateGlobalForm]
        });
    }

    getThreshold() : FormArray {
        return this.promotionForm.get(PromotionAddComponent.formNames.thresholds) as FormArray;
    }

    getThresholdFormGroup(index: number) : FormGroup {

        return this.getThreshold().controls[index] as FormGroup;
    }

    getMinAmountFromControl(index: number): FormControl {
        return (this.getThreshold().controls[index] as FormGroup).get(PromotionAddComponent.formNames.minAmount) as FormControl;
    }

    getMaxAmountFromControl(index: number): FormControl {
        return (this.getThreshold().controls[index] as FormGroup).get(PromotionAddComponent.formNames.maxAmount) as FormControl;
    }

    amountGreaterThan(controlName: string): ValidatorFn {

        return (control: FormControl) : {[key: string]: any} | null => {
            
            let parent: FormGroup = control.parent as FormGroup;
            if(parent != null) {

                let compareCtrl: FormControl = parent.get(controlName) as FormControl;

                let amountRegex: RegExp = new RegExp('[0-9]+'); 
                if(!compareCtrl.invalid && amountRegex.test(compareCtrl.value) && amountRegex.test(control.value)) {

                    let minAmount: number = +compareCtrl.value;
                    let maxAmount: number = +control.value;

                    if(minAmount >= maxAmount) {

                        return { 'mustGreaterThan': true };
                    }
                }
            }

            return null;
        }
    }

    validateGlobalForm: ValidatorFn = (control: FormGroup): ValidationErrors | null => {

        let thresholdFormArray: FormArray = control.get(PromotionAddComponent.formNames.thresholds) as FormArray;
        if(thresholdFormArray.length === 0) {

            return { atLeastOne: true };
        }        

        return null;
    }  

    validateThreshold: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
        
        return null;

        // console.log('validateThreshold in');
        
        // let minMaxHasError: boolean = false;
        // let feesHasError: boolean = false;

        // const minAmountCtrl = control.get('minAmount');
        // const maxAmountCtrl = control.get('maxAmount');
        
        // console.log(minAmountCtrl);
        // console.log(maxAmountCtrl);

        // if(minAmountCtrl && maxAmountCtrl) {

        //     if(minAmountCtrl.hasError('minMustLessThanMax')) {
        //         delete minAmountCtrl.errors.minMustLessThanMax;
        //     }

        //     if(maxAmountCtrl.hasError('minMustLessThanMax')) {
        //         delete maxAmountCtrl.errors.minMustLessThanMax;
        //     }

        //     if((minAmountCtrl.errors == null || (minAmountCtrl.errors != null && minAmountCtrl.errors.length == 0))
        //        && (maxAmountCtrl.errors == null || (maxAmountCtrl.errors != null && maxAmountCtrl.errors.length == 0)))
        //     {
        //         let minAmount: number = +minAmountCtrl.value;
        //         let maxAmount: number = +maxAmountCtrl.value;

        //         if(minAmount >= maxAmount) {

        //             minAmountCtrl.setErrors({ 'minMustLessThanMax': true });
        //             maxAmountCtrl.setErrors({ 'minMustLessThanMax': true });

        //             minMaxHasError = true;
        //         }
        //     }
        // }

        // const shippingFeesFormControl = control.get('shippingDiscountRate');
        // const serviceFeesFormControl = control.get('serviceDiscountRate');

        // if(shippingFeesFormControl 
        //    && serviceFeesFormControl 
        //    && !shippingFeesFormControl.invalid 
        //    && !serviceFeesFormControl.invalid) {

        //     if(shippingFeesFormControl.value == '' && serviceFeesFormControl.value == '') {

        //         shippingFeesFormControl.setErrors({ 'feesRequired' : true });
        //         serviceFeesFormControl.setErrors({ 'feesRequired' : true });

        //         feesHasError = true;
        //     }
        // }

        // let errors: ValidationErrors = {
        // };

        // if(minMaxHasError || feesHasError) {
            
        //     let errors: ValidationErrors = {};
            
        //     if(minMaxHasError) {
        //         errors.minMustLessThanMax = true;
        //     }
        //     if(feesHasError) {
        //         errors.feesRequired = true;
        //     }

        //     return errors;
        // }

        // return null;
      };

    addThreshold() {

        let minAmount: FormControl = new FormControl('', [Validators.required, Validators.pattern('[0-9]*')]);
        let maxAmount: FormControl = new FormControl('', [Validators.required, Validators.pattern('[0-9]*'), this.amountGreaterThan('minAmount')]);
        minAmount.valueChanges.subscribe(() => {

            maxAmount.updateValueAndValidity();
        });

        let newFormGroup: FormGroup = this.formBuilder.group({
            minAmount: minAmount,
            maxAmount: maxAmount,
            shippingDiscountRate: ['', [Validators.pattern('[0-9]*')]],
            serviceDiscountRate: ['', [Validators.pattern('[0-9]*')]]
        }, { 
            validators: this.validateThreshold
        });
        this.getThreshold().push(newFormGroup);
    }

    deleteThreshold(index: number) {
        this.getThreshold().controls = this.getThreshold().controls.splice(index, 1);
    }

    submissionValidate() {

        let thresholdFormArray: FormArray = this.promotionForm.get(PromotionAddComponent.formNames.thresholds) as FormArray;
        if(thresholdFormArray.length === 0) {
            this.promotionForm.setErrors({
                atLeastOne: true
            });

            return;
        }
    }

    onSubmitForm() {

        console.log(this.promotionForm);

        this.submissionValidate();

        if(this.promotionForm.invalid) {
            return;
        }

        const formValue = this.promotionForm.value;

        let newPromotion: PromotionModel = new PromotionModel();
        newPromotion.campaignId = '1';
        newPromotion.enable = formValue.enable;
        newPromotion.endValidityDate = new Date(formValue.endValidityDate);
        newPromotion.name = formValue.name;
        newPromotion.promoCodeId = formValue.promoCodeId;
        newPromotion.promotionCode = formValue.promotionCode;
        newPromotion.startValidityDate = new Date(formValue.startValidityDate);
        newPromotion.thresholds = [];

        if(formValue.thresholds != null) {

            for(let threshold of formValue.thresholds) {
                let newThreshold: ThresholdModel = new ThresholdModel();
                newThreshold.minThresholdAmount = threshold.minAmount;
                newThreshold.maxThresholdAmount = threshold.maxAmount;
                newThreshold.discountItems = [];

                if(threshold.shippingDiscountRate != null) {
                    let discount: DiscountItemModel = new DiscountItemModel();
                    discount.discountAmount = parseInt(threshold.shippingDiscountRate);
                    discount.discountType = DiscountType.Shipping;

                    newThreshold.discountItems.push(discount);
                }

                if(threshold.serviceDiscountRate != null) {
                    let discount: DiscountItemModel = new DiscountItemModel();
                    discount.discountAmount = parseInt(threshold.shippingDiscountRate);
                    discount.discountType = DiscountType.Shipping;

                    newThreshold.discountItems.push(discount);
                }
            }
        }

        this.promotionService.create(newPromotion);

        this.router.navigate(['/promotions']);
    }
}
