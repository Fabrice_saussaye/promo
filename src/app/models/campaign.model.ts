import { PromotionModel } from './promotion.model';

export class CampaignModel {

    campaignId: string;
    name: string;
    enable: boolean;
    startValidityDate: Date;
    endValidityDate: Date;
    createdBy: string;

    promotions: PromotionModel[];
}