import { DiscountItemModel } from './discount-item.model';

export class ThresholdModel {
    minThresholdAmount: number;
    maxThresholdAmount: number;

    discountItems: DiscountItemModel[];
}