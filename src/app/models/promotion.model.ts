import { ThresholdModel } from './threshold.model';

export class PromotionModel {
    campaignId: string;
    promoCodeId: string;
    promotionCode: string;
    name: string;
    enable: boolean;
    startValidityDate: Date;
    endValidityDate: Date;
    createdBy: string;

    thresholds: ThresholdModel[];
}