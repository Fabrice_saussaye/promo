import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CampaignModel } from "./../models/campaign.model";

import { CampaignService } from "./../services/campaign.service";
import { _MatCheckboxMixinBase } from '@angular/material';

@Component({
  selector: 'app-campaign-list',
  templateUrl: './campaign-list.component.html',
  styleUrls: ['./campaign-list.component.css']
})
export class CampaignListComponent implements OnInit {

  campaigns: CampaignModel[];

  constructor(
    private campaignService: CampaignService,
    private router: Router) { }

    displayedColumns: string[] = ['Name', 'StartValidityDate', 'EndValidityDate', 'Enable'];
    dataSource: CampaignModel[] = [];
    loaded: boolean = false;


  ngOnInit() {

    this.campaigns = this.campaignService.campaigns;

    this.loadCampaign();
  }

  loadCampaign = () => {

    this.campaignService.getAsync().subscribe(
        (campaigns: CampaignModel[]) => {

            console.log(campaigns);

            this.dataSource = campaigns;
            this.loaded = true;
        },
        (error) => {
            alert("An error occured !!!");
            console.log(error);
        } 
    );
  }

  getDetailLink = (campaignId: string) => {

    return '/campaignDetail/' + campaignId;
  }

  seePromotions = (campaignId: string) => {

    this.router.navigate(['promotions', campaignId]);
  }

}
