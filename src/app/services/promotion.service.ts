import { PromotionModel } from '../models/promotion.model';

export class PromotionService {

    private promotions: PromotionModel[] = [
        {
            campaignId: '1',
            createdBy: 'toto',
            endValidityDate: new Date(2019, 1, 1),
            enable: true,
            name: 'Promotion 10%',
            promoCodeId: '1',
            promotionCode: 'PROMO10',
            startValidityDate: new Date(2019, 6, 1),
            thresholds: []
        },
        {
            campaignId: '1',
            createdBy: 'titi',
            endValidityDate: new Date(2019, 1, 1),
            enable: false,
            name: 'Promotion 20%',
            promoCodeId: '2',
            promotionCode: 'PROMO20',
            startValidityDate: new Date(2019, 6, 1),
            thresholds: []
        },
        {
            campaignId: '2',
            createdBy: 'tata',
            endValidityDate: new Date(2019, 1, 1),
            enable: true,
            name: 'Promotion 30%',
            promoCodeId: '1',
            promotionCode: 'PROMO30',
            startValidityDate: new Date(2019, 6, 1),
            thresholds: []
        }
    ];

    getByCampaignId(campaignId: string): PromotionModel[] {

        return this.promotions.filter(p => p.campaignId === campaignId);
    }

    getAll() : PromotionModel[] {

        return this.promotions;
    }

    create(promotion: PromotionModel): boolean {

        this.promotions.push(promotion);

        return true;
    }
}