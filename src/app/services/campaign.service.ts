import { CampaignModel } from './../models/campaign.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class CampaignService {

    private baseUrl: string = 'http://localhost:63193/api/v1/campaign';
    private httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
      };

    campaigns: CampaignModel[] = [
        {
            campaignId: '1',
            name: 'first Campaign',
            startValidityDate: new Date(2018, 1, 1),
            endValidityDate: new Date(2018, 2, 1),
            createdBy: 'toto',
            enable: false,
            promotions: []
        },
        {
            campaignId: '2',
            name: 'second Campaign',
            startValidityDate: new Date(2018, 3, 1),
            endValidityDate: new Date(2018, 4, 1),
            createdBy: 'toto',
            enable: true,
            promotions: []
        },
        {
            campaignId: '3',
            name: 'Campaign 3',
            startValidityDate: new Date(2018, 5, 1),
            endValidityDate: new Date(2018, 6, 1),
            createdBy: 'toto',
            enable: true,
            promotions: []
        }
    ];

    constructor(
        private http : HttpClient) { }

    /**
     * Add Campaign through campaign API
     */
    addAsync = (campaign: CampaignModel): Observable<any> => {

        return this.http.post(this.baseUrl, campaign, this.httpOptions);
    }

    /**
     * Get All campaign through campaign API
     */
    getAsync = () : Observable<CampaignModel[]> => {

        return this.http.get<CampaignModel[]>(this.baseUrl, this.httpOptions);
    }

    getByIdAsync = (campaignId: string): Observable<CampaignModel> => {

        return this.http.get<CampaignModel>(this.baseUrl + '/' + campaignId, this.httpOptions);
    }

    getDetailByIdAsync = (campaignId: string): Observable<CampaignModel> => {

        return this.http.get<CampaignModel>(this.baseUrl + '/' + campaignId + '/detail', this.httpOptions);
    }

    getById = (campaignId: string) => {

        return this.campaigns.find((campaign) => campaign.campaignId === campaignId);
    };

    add = (campaign: CampaignModel) => {

        this.campaigns.push(campaign);
    };

    exist = (campaignId: string) => {

        return (this.getById(campaignId) != null);
    };

    update = (campaign: CampaignModel) => {

        
    }
}