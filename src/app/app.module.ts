import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// https://www.npmjs.com/package/ng-pick-datetime
//import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

// Angular Material
import { MatButtonModule, MatCheckboxModule, MatInputModule, MatFormFieldModule, MatNativeDateModule } from '@angular/material';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatTableModule } from '@angular/material/table';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthentificationComponent } from './authentification/authentification.component';
import { CampaignListComponent } from './campaign-list/campaign-list.component';

import { CampaignService } from './services/campaign.service';
import { CampaignEditComponent } from './campaign-edit/campaign-edit.component';
import { CampaignAddComponent } from './campaign-add/campaign-add.component';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './routes/guards/auth.guard';
import { HeaderMenuComponent } from './header-menu/header-menu.component';
import { PromotionListComponent } from './promotion-list/promotion-list.component';
import { PromotionAddComponent } from './promotion-add/promotion-add.component';
import { PromotionService } from './services/promotion.service';
import { PromotionEditComponent } from './promotion-edit/promotion-edit.component';
import { CampaignDetailComponent } from './campaign-detail/campaign-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthentificationComponent,
    CampaignListComponent,
    CampaignEditComponent,
    CampaignAddComponent,
    HeaderMenuComponent,
    PromotionListComponent,
    PromotionAddComponent,
    PromotionEditComponent,
    CampaignDetailComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    /*OwlDateTimeModule,
    OwlNativeDateTimeModule,*/
    MatButtonModule, 
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule,
    MatListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MatTableModule,
    MatSlideToggleModule
  ],
  providers: [
    CampaignService,
    PromotionService,
    AuthService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
