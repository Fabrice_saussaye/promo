import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

import {MatMenuModule} from '@angular/material/menu';

@Component({
  selector: 'app-header-menu',
  templateUrl: './header-menu.component.html',
  styleUrls: ['./header-menu.component.css']
})
export class HeaderMenuComponent implements OnInit {

  isAuth: boolean;

  constructor(
    private authService: AuthService,
    private router: Router) { }

  ngOnInit() {

    this.isAuth = this.authService.isAuth;
  }

  login() {

    this.router.navigate(['auth']);
  }

  logout() {

    this.authService.logout();

    this.router.navigate(['auth']);
  }
}
