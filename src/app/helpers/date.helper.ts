export class DateHelper {

    static toFormatDate(date: Date) {

        return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    }

    static parseDate(dateString: string) {

        let splittedDate: string[] = dateString.split('/');
        if(splittedDate.length === 3) {
            let day: number = parseInt(splittedDate[0]);
            let month: number = parseInt(splittedDate[1]);
            let year: number = parseInt(splittedDate[2]);

            let tempDate: Date = new Date(year, month - 1, day);
            if(tempDate.getFullYear() === year
                && tempDate.getMonth() === (month-1)
                && tempDate.getDate() === day)
            {
                return tempDate; 
            }
        }

        return null;
    }
}