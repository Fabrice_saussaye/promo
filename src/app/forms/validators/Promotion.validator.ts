import { ValidatorFn, FormGroup, ValidationErrors } from '@angular/forms/src/forms';

export class PromotionValidators {

    static validateThreshold: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
        
        console.log('validateThreshold in');
        
        const minAmountCtrl = control.get('minAmount');
        const maxAmountCtrl = control.get('maxAMount');
      
        if(minAmountCtrl && maxAmountCtrl && !minAmountCtrl.invalid && !maxAmountCtrl.invalid) {

            let minAmount: number = +minAmountCtrl.value;
            let maxAmount: number = +maxAmountCtrl.value;

            minAmountCtrl.setErrors({ 'minMustLessThanMax': true });
            maxAmountCtrl.setErrors({ 'minMustLessThanMax': true });

            if(minAmount >= maxAmount) {

                return { 'minMustLessThanMax' : true };
            }
        }

        const shippingFeesFormControl = control.get('shippingDiscountRate');
        const serviceFeesFormControl = control.get('serviceDiscountRate');

        if(shippingFeesFormControl 
           && serviceFeesFormControl 
           && !shippingFeesFormControl.invalid 
           && !serviceFeesFormControl.invalid) {

            if(shippingFeesFormControl.value == '' && serviceFeesFormControl.value == '') {

                shippingFeesFormControl.setErrors({ 'feesRequired' : true });
                serviceFeesFormControl.setErrors({ 'feesRequired' : true });

                return { 'feesRequired' : true };
            }
        }

        return null;
      };
}