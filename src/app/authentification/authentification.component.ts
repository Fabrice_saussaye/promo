import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-authentification',
  templateUrl: './authentification.component.html',
  styleUrls: ['./authentification.component.css']
})
export class AuthentificationComponent implements OnInit {

  static formNames = {
    username: 'username',
    password: 'password'
  };

  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmitForm() {

    const formValues = this.loginForm.value;
    const usernameValue = formValues[AuthentificationComponent.formNames.username];
    const passwordValue = formValues[AuthentificationComponent.formNames.password];

    if(this.authService.login(usernameValue, passwordValue)) {

      this.router.navigate(['/']);
    }
    else {

      this.loginForm.controls[AuthentificationComponent.formNames.username].setErrors({
        invalidCrdentials: true
      });
    }
  }
}
