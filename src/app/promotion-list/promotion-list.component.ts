import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PromotionModel } from '../models/promotion.model';
import { PromotionService } from '../services/promotion.service';

@Component({
  selector: 'app-promotion-list',
  templateUrl: './promotion-list.component.html',
  styleUrls: ['./promotion-list.component.css']
})
export class PromotionListComponent implements OnInit {

  campaignId: string = null;
  promotions: PromotionModel[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private promotionService: PromotionService,
    private router: Router) { }

  ngOnInit() {

    console.log(this.activatedRoute.snapshot.params);

    if(this.activatedRoute.snapshot.params.hasOwnProperty("campaignId")) {
      this.campaignId = this.activatedRoute.snapshot.params['campaignId'];
      this.promotions = this.promotionService.getByCampaignId(this.campaignId);
    } 
    else {
      this.promotions = this.promotionService.getAll();
    }
  }

  edit(codePromoId: string) {

    this.router.navigate(['promotionEdit', codePromoId]);
  }
}
