import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';

import { CampaignService } from './../services/campaign.service';
import { CampaignModel } from '../models/campaign.model';
import { DateHelper } from '../helpers/date.helper';

@Component({
  selector: 'app-campaign-add',
  templateUrl: './campaign-add.component.html',
  styleUrls: ['./campaign-add.component.css']
})
export class CampaignAddComponent implements OnInit {

    static formNames = {
        campaignId: 'campaignId',
        name: 'name',
        enable: 'enable',
        startValidityDate: 'startValidityDate',
        endValidityDate: 'endValidityDate'
    };

    campaignForm: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private campaignService: CampaignService,
        private router: Router) { }

    ngOnInit() {
        this.initForm();
    }

    /**
     * Validate that the start date must be less than end date
     * @param controlName start date control form name
     */
    dateGreaterThan(controlName: string): ValidatorFn {

        return (control: FormControl) : {[key: string]: any} | null => {
            
            console.log('dateGreaterThan');

            let parent: FormGroup = control.parent as FormGroup;
            if(parent != null) {

                let compareCtrl: FormControl = parent.get(controlName) as FormControl;

                if(!compareCtrl.invalid) {

                    let startDate = new Date(compareCtrl.value);
                    let endDate = new Date(control.value);
                    endDate.setHours(23);
                    endDate.setMinutes(59);
                    endDate.setSeconds(59);

                    if(startDate >= endDate) {

                        return { 'mustGreaterThan': true };
                    }
                }
            }

            return null;
        }
    }

    initForm() {

        let startValidityDateCtrl: FormControl = new FormControl('', [Validators.required]);
        let endValidityDateCtrl: FormControl = new FormControl('', [Validators.required, this.dateGreaterThan(CampaignAddComponent.formNames.startValidityDate)]);
        startValidityDateCtrl.valueChanges.subscribe(
            () => {
                endValidityDateCtrl.updateValueAndValidity();
            }
        );

        this.campaignForm = this.formBuilder.group({
            name: ['', Validators.required],
            enable: [false],
            startValidityDate: startValidityDateCtrl,
            endValidityDate: endValidityDateCtrl
        });
    };

    onSubmitForm() {

        const formValue = this.campaignForm.value;
        const nameValue: string = formValue[CampaignAddComponent.formNames.name];
        const enableValue: boolean = formValue[CampaignAddComponent.formNames.enable];
        const startValidityDateValue: string = formValue[CampaignAddComponent.formNames.startValidityDate];
        const endValidityDateValue: string = formValue[CampaignAddComponent.formNames.endValidityDate];

        if(this.campaignForm.invalid) {
            return;
        }
        else {

            let startValidityDate = new Date(startValidityDateValue);
            let endValidityDate = new Date(endValidityDateValue);
            endValidityDate.setHours(23);
            endValidityDate.setMinutes(59);
            endValidityDate.setSeconds(59);

            let newCampaign = new CampaignModel();
            newCampaign.name = nameValue;
            newCampaign.enable = enableValue;
            newCampaign.startValidityDate = startValidityDate;
            newCampaign.endValidityDate = endValidityDate;
      
            //this.campaignService.add(newCampaign);

            this.campaignService.addAsync(newCampaign).subscribe(
                () => {
                    this.router.navigate(["/"]);
                },
                () => {
                    alert("Erreur lors de la création d'une campagne");
                }
            );
        }
    };

    getNameErrorMessage() {
        if(this.name.hasError('required')) {
            return 'The name is required';
        }
    }

    getStartValidityDateErrorMessage() {
        if(this.startValidityDate.hasError('required')) {
            return 'The start validity date is required';
        }
    }

    getEndValidityDateErrorMessage() {
        if(this.endValidityDate.hasError('required')) {
            return 'The end validity date is required';
        }
        else if (this.endValidityDate.hasError('mustGreaterThan')) {
            return 'the end validity date must be greater than start validity date';
        }
    }

    get name(): FormControl { return this.campaignForm.get(CampaignAddComponent.formNames.name) as FormControl; }
    get startValidityDate(): FormControl { return this.campaignForm.get(CampaignAddComponent.formNames.startValidityDate) as FormControl; }
    get endValidityDate(): FormControl { return this.campaignForm.get(CampaignAddComponent.formNames.endValidityDate) as FormControl; }

}
