import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { CampaignModel } from './../models/campaign.model';

import { CampaignService } from './../services/campaign.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DateHelper } from '../helpers/date.helper';

@Component({
  selector: 'app-campaign-edit',
  templateUrl: './campaign-edit.component.html',
  styleUrls: ['./campaign-edit.component.css']
})
export class CampaignEditComponent implements OnInit {

  static formNames = {
    campaignId: 'campaignId',
    name: 'name',
    enable: 'enable',
    startValidityDate: 'startValidityDate',
    endValidityDate: 'endValidityDate'
  }

  campaign: CampaignModel = null;
  campaignForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private campaignService: CampaignService,
    private formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit() {

    const campaignId: string = this.route.snapshot.params['id'];
    this.campaign = this.campaignService.getById(campaignId);

    this.initForm();
  }

  initForm() {

    this.campaignForm = this.formBuilder.group({
      campaignId: [this.campaign.campaignId, [Validators.required, Validators.pattern('[0-9]+')]],
      name: [this.campaign.name, Validators.required],
      enable: [this.campaign.enable],
      startValidityDate: [DateHelper.toFormatDate(this.campaign.startValidityDate), Validators.required],
      endValidityDate: [DateHelper.toFormatDate(this.campaign.endValidityDate), Validators.required]
    });
  }

  onSubmitForm() {

    const formValue = this.campaignForm.value;
    const campaignId = formValue[CampaignEditComponent.formNames.campaignId];
    const name = formValue[CampaignEditComponent.formNames.name];
    const enable = formValue[CampaignEditComponent.formNames.enable];
    const startValidityDate = formValue[CampaignEditComponent.formNames.startValidityDate];
    const endValidityDate = formValue[CampaignEditComponent.formNames.endValidityDate];

    if(this.campaignService.exist(campaignId)) {

      this.campaignForm.controls[CampaignEditComponent.formNames.campaignId].setErrors({
        notUnique: true
      });
    }

    let tempStartDate : Date = DateHelper.parseDate(startValidityDate);
    let tempEndDate: Date = DateHelper.parseDate(endValidityDate);

    if(tempStartDate === null) {
      this.campaignForm.controls[CampaignEditComponent.formNames.startValidityDate].setErrors({
        notvalid: true
      });
    }

    if(tempEndDate === null) {
      this.campaignForm.controls[CampaignEditComponent.formNames.endValidityDate].setErrors({
        notvalid: true
      });
    }

    if(tempStartDate != null && tempEndDate != null && (tempStartDate >= tempEndDate))
    {
      this.campaignForm.controls[CampaignEditComponent.formNames.endValidityDate].setErrors({
        mustBeSuperior: true
      });
    }

    if(this.campaignForm.invalid) {
      return;
    }
    else {
      let newCampaign = new CampaignModel();
      this.campaign.campaignId = campaignId;
      this.campaign.name = name;
      this.campaign.enable = enable;
      this.campaign.startValidityDate = tempStartDate;
      this.campaign.endValidityDate = tempEndDate;
      
      this.campaignService.update(newCampaign);
      
      this.router.navigate(["/"]);
    }
  };

  get campaignId() { return this.campaignForm.get(CampaignEditComponent.formNames.campaignId); }
}
