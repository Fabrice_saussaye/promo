import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthentificationComponent } from './authentification/authentification.component';
import { CampaignListComponent } from './campaign-list/campaign-list.component';
import { CampaignAddComponent } from './campaign-add/campaign-add.component';
import { CampaignDetailComponent } from './campaign-detail/campaign-detail.component';
import { CampaignEditComponent } from "./campaign-edit/campaign-edit.component";
import { AuthGuard } from './routes/guards/auth.guard';
import { PromotionListComponent } from './promotion-list/promotion-list.component';
import { PromotionAddComponent } from './promotion-add/promotion-add.component';
import { PromotionEditComponent } from './promotion-edit/promotion-edit.component';

const routes: Routes = [
  { path: 'auth', component: AuthentificationComponent },
  { path: 'campaignAdd', canActivate: [AuthGuard], component: CampaignAddComponent },
  { path: 'campaignDetail/:campaignId', canActivate: [AuthGuard], component: CampaignDetailComponent },
  { path: 'campaignEdit/:id', canActivate: [AuthGuard], component: CampaignEditComponent },
  { path: 'promotions', canActivate: [AuthGuard], component: PromotionListComponent },
  { path: 'promotions/:campaignId', canActivate: [AuthGuard], component: PromotionListComponent },
  { path: 'promotionAdd', canActivate: [AuthGuard], component: PromotionAddComponent },
  { path: 'promotionEdit/:id', canActivate: [AuthGuard], component: PromotionEditComponent },
  { path: '', canActivate: [AuthGuard], component: CampaignListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
