import { Component, OnInit } from '@angular/core';
import { CampaignService } from '../services/campaign.service';
import { PromotionModel } from '../models/promotion.model';
import { CampaignModel } from '../models/campaign.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-campaign-detail',
    templateUrl: './campaign-detail.component.html',
    styleUrls: ['./campaign-detail.component.css']
})
export class CampaignDetailComponent implements OnInit {

    private loaded: boolean = false;
    campaign: CampaignModel = null;
    dataSource: PromotionModel[] = [];

    constructor(
        private campaignService: CampaignService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {

        this.load();
    }

    load() {

        this.campaignService.getDetailByIdAsync(this.route.snapshot.params['campaignId'])
            .subscribe(
                (campaign: CampaignModel) => {

                    this.campaign = campaign;
                    this.dataSource = campaign.promotions;
                    this.loaded = true;
                },
                (error) => {
                    alert('an error occured');
                }
            );
    }
}
